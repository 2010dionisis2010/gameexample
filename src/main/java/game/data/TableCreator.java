package game.data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TableCreator {

    private static String fileName = "migration.sql";

    public static void main(String[] args) throws SQLException, IOException {
        ConnectionFactory postgreConnectionFactory = ConnectionFactory.POSTGRE_CONNECTION_FACTORY;
        Connection connection = postgreConnectionFactory.getConnection();

        URL resource = TableCreator.class.getClassLoader().getResource(fileName);
        BufferedReader reader = new BufferedReader(new FileReader(resource.getFile()));
        String query = reader.readLine();
        reader.close();

        PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();



    }
}
