package game.data;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public enum ConnectionFactory {

    POSTGRE_CONNECTION_FACTORY;

    private DataSource dataSource;
    private String password ="postgres";
    private String login = "postgres";

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/game", login, password);
    }
}
