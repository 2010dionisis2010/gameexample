package game.data.dao;

import game.entity.Entity;

import java.sql.SQLException;

public interface EntityDao {

    boolean save(Entity entity) throws SQLException;
}
