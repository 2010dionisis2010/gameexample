package game.data.dao;

import game.data.ConnectionFactory;
import game.entity.Entity;
import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log4j2
public class EntityDaoPostgres implements EntityDao {

    private Connection connection;

    public EntityDaoPostgres(Connection connection) {
        this.connection = connection;
    }

    public boolean save(Entity entity) {
        try {
            String query = String.format("insert into entity values(%s)", getSpacesForParam(5));
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setLong(1, 1);
            statement.setLong(2, entity.getId());
            statement.setInt(3, entity.getLocation().getX());
            statement.setInt(4, entity.getLocation().getY());
            statement.setInt(5, entity.getHp());
            statement.execute();
            return true;
        } catch (SQLException throwables) {
            log.error("Save failed ");
            return false;
        }
    }

    private String getSpacesForParam(int quantity) {
        return Stream.generate(() -> "?").limit(quantity).collect(Collectors.joining(", "));
    }
}
