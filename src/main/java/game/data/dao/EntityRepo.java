package game.data.dao;

import game.entity.Entity;
import lombok.extern.log4j.Log4j2;

import java.sql.SQLException;

@Log4j2
public class EntityRepo {

    private EntityDao entityDao;

    public boolean save(Entity entity) {
        try {
            return entityDao.save(entity);
        } catch (SQLException throwables) {
            log.error(throwables);
            return false;
        }
    }

}
