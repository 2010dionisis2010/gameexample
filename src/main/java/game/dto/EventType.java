package game.dto;

import java.io.Serializable;
import java.util.Map;

public enum EventType implements Serializable {
    UPDATE,
    START;
}
