package game.dto;

import lombok.Getter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Getter
public class Event implements Serializable {

    private EventType type;

    private Map<Param, Object> args = new HashMap<>();

    public void put(Param name, Object value) {
        args.put(name, value);
    }
    public Object get(Param name) {
        return args.get(name);
    }
}
