package game.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Param {

    GAME,
    STATE;
}
