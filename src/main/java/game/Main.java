package game;

import org.omg.PortableServer.THREAD_POLICY_ID;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Runnable runnable = () -> {
            Thread currentThread = Thread.currentThread();
            while (!currentThread.isInterrupted()) {
                System.out.println(currentThread.getName());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.err.println("1234235");
                    break;
                }
            }

            System.err.println("Child THread was stopped");
        };
        Thread thread = new Thread(runnable);
        Thread thread2 = new Thread(runnable);
//        thread.setDaemon(true);
        thread.start();
        thread2.start();

        Thread.sleep(3000);

        thread.interrupt();

        System.out.println("main work");

    }
}
