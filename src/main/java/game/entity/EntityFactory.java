package game.entity;

import game.engine.CreateEntityException;
import game.entity.enemy.Enemy;
import game.entity.person.Person;
import lombok.extern.log4j.Log4j2;

@Log4j2
public enum  EntityFactory {
    MAIN_FACTORY;

    private long nextPersonID = 0;
    private long nextEnemyID = 0;

    public Person createPerson() {
        Person person = new Person(++nextPersonID);
        log.info(String.format("person %d created", person.getId()));
        return person;
    }

    public Enemy createEnemy() {
        Enemy enemy = new Enemy(++nextEnemyID);
        log.info(String.format("enemy %d created", enemy.getId()));
        return enemy;
    }

    public <T extends Entity> Entity createEntity(Class<T> aClass) {
        try {
            T entity = aClass.newInstance();
            log.info(String.format("entity %d created", entity.getId()));
            return entity;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new CreateEntityException(aClass);
        }
    }
}
