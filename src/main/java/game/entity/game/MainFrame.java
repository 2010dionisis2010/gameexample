package game.entity.game;

import game.engine.EntityView;
import game.entity.gamefield.FieldView;
import lombok.Getter;

import javax.swing.*;
import java.util.List;


@Getter
public class MainFrame {

    private int width;
    private int height;

    private JFrame window = new JFrame();
    private FieldView fieldView;
    private List<EntityView> entityViewList;

//    private JButton update = new JButton("Update");

    public MainFrame(int width, int height, FieldView fieldView, List<EntityView> entityViewList) {
        this.width = width;
        this.height = height;
//        this.update.setLocation(300, 10);
//        fieldView.getPanel().add(update);
//        update.setVisible(true);

        this.fieldView = fieldView;
        this.entityViewList = entityViewList;
        window.add(fieldView.getPanel());
        fieldView.add(entityViewList);

        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    public void show(){
        window.pack();
        window.setVisible(true);
    }

    public void reRender(){
        window.repaint();
    }
}
