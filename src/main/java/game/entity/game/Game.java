package game.entity.game;

import game.engine.Location;
import game.entity.Configuration;
import game.entity.Entity;
import game.entity.EntityFactory;
import game.entity.enemy.EnemyView;
import game.entity.gamefield.GameField;
import game.entity.enemy.Enemy;
import game.entity.person.Person;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

@NoArgsConstructor
@Log4j2
@Getter
public class Game implements Serializable {

    private EntityFactory entityFactory = EntityFactory.MAIN_FACTORY;

    private long id;

    private GameField gameField;
    private List<Entity> entityList;
    private List<Enemy> enemyList;
    private Person person;
    private Configuration configuration;

    public Game(Configuration configuration) {
        this.gameField = new GameField(configuration.getFieldWidth(), configuration.getFieldHeight());
        this.configuration = configuration;
        createEntities(this.configuration);
        this.person = entityFactory.createPerson();

        log.info(String.format("game %d created", id));
    }

    private void createEntities(Configuration configuration) {
        enemyList = new ArrayList<>();
        entityList = new ArrayList<>();
        for (int i = 0; i < configuration.getEnemiesQuantity(); i++) {
            Enemy enemy = entityFactory.createEnemy();
            enemyList.add(enemy);
            entityList.addAll(enemyList);
        }
    }

    public void randomMoveAllEnemies() {
        entityList.forEach(entity -> entity.setLocation(getRandomLocation()));
    }

    private Location getRandomLocation() {
        return new Location(new Random().nextInt(gameField.getWidth() - 50),
                new Random().nextInt(gameField.getHeight() - 50));
    }

    public void updateState(Game game) {
        Map<Long, Entity> newEntities = game.getEntityList().stream().collect(Collectors.toMap(Entity::getId, entity -> entity));
        this.getEntityList().forEach(entity -> entity.setLocation(newEntities.get(entity.getId()).getLocation()));
    }
}
