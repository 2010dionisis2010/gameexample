package game.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Configuration implements Serializable {

    private int enemiesQuantity;
    private int fieldWidth;
    private int fieldHeight;

    public static Configuration getDefault(){
        return new Configuration(5, 1000, 1000);
    }
}
