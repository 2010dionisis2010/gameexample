package game.entity.gamefield;

import game.engine.EntityView;
import game.engine.View;
import lombok.Getter;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class FieldView extends View {

    @Getter
    private JPanel panel;

    public FieldView(String name, int width, int height) {
        super(name, width, height);
        this.panel = new JPanel(null);
        panel.setPreferredSize(new Dimension(width, height));
    }

    public void add(List<EntityView> entityViewList) {
        entityViewList.forEach(entityView -> panel.add(entityView.getSwingView()));
    }
}
