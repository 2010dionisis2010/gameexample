package game.entity.gamefield;

import game.engine.View;
import game.entity.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
public class GameField implements Serializable {

    private int width = 1000;
    private int height = 1000;
    private View view;

    private List<Entity> entityList = new ArrayList<>();

    public GameField(int width, int height) {
        this.width = width;
        this.height = height;
    }


    public void addEntity(Entity entity){
        entityList.add(entity);
    }


}
