package game.entity;

import game.engine.Locatable;
import game.engine.Location;
import game.engine.View;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@NoArgsConstructor
public class Entity implements Locatable, Serializable {

    protected long id;
    @Setter
    protected Location location = new Location(0, 0);
    protected int hp;

    @Setter
    private View view;

    public boolean isAlive() {
        return hp > 0;
    }

    public Entity(long id) {
        this.id = id;
    }
}
