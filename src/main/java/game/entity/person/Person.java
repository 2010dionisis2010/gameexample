package game.entity.person;

import game.entity.Entity;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Person extends Entity {

    public Person(long id) {
        super(id);
    }
}
