package game.engine;

import game.entity.Configuration;
import game.entity.Entity;
import game.entity.game.Game;
import game.entity.game.MainFrame;
import game.entity.gamefield.FieldView;
import game.entity.gamefield.GameField;
import game.entity.person.Person;
import game.entity.enemy.Enemy;
import game.entity.enemy.EnemyView;
import game.entity.person.PersonView;

import javax.swing.*;
import java.util.List;
import java.util.stream.Collectors;

public enum ViewFactory {

    MAIN_VIEW_FACTORY;

    public static int DEFAULT_ENTITY_WIDTH = 50;
    public static int DEFAULT_ENTITY_HEIGHT = 50;

    public EnemyView create(Enemy enemy) {
        EnemyView enemyView = new EnemyView(MAIN_VIEW_FACTORY.createName(enemy), DEFAULT_ENTITY_WIDTH, DEFAULT_ENTITY_HEIGHT);
        Location location = enemy.getLocation();
        enemyView.getSwingView().setLocation(location.getX(), location.getY());
        enemyView.setId(enemy.getId());
        return enemyView;
    }

    public PersonView create(Person person) {
        PersonView view = new PersonView(MAIN_VIEW_FACTORY.createName(person), DEFAULT_ENTITY_WIDTH, DEFAULT_ENTITY_HEIGHT);
        Location location = person.getLocation();
        view.getSwingView().setLocation(location.getX(), location.getY());
        view.setId(person.getId());
        return view;
    }

    public FieldView create(GameField field) {
        return new FieldView("field", field.getWidth(), field.getHeight());
    }

    public MainFrame create(Game game) {
        Configuration configuration = game.getConfiguration();
        return new MainFrame(configuration.getFieldWidth(),
                configuration.getFieldHeight(),
                MAIN_VIEW_FACTORY.create(game.getGameField()),
                MAIN_VIEW_FACTORY.map(game.getEntityList()));
    }

    private List<EntityView> map(List<Entity> entities) {
        return entities.stream()
                .map(MAIN_VIEW_FACTORY::map)
                .collect(Collectors.toList());
    }

    public EntityView map(Entity entity) {
        if (entity instanceof Person){
            return MAIN_VIEW_FACTORY.create((Person) entity);
        } else if (entity instanceof Enemy){
            return MAIN_VIEW_FACTORY.create((Enemy) entity);
        } else {
            throw new IllegalArgumentException(String.format("Не известынй тип Entity %s", entity.getClass()));
        }
    }

    private String createName(Entity enemy) {
        return String.format("%s %s", enemy.getClass().getSimpleName(), enemy.getId());
    }


}
