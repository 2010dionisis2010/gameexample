package game.engine;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Location implements Serializable {

    private int x;
    private int y;

    public int changeX(int delta) {
        return x += delta;
    }

    public int changeY(int delta) {
        return y += delta;
    }


}
