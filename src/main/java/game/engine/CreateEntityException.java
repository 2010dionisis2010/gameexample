package game.engine;

public class   CreateEntityException extends RuntimeException {


    private Class aClass;

    public <T> CreateEntityException(Class<T> aClass) {
        this.aClass = aClass;
    }

    @Override
    public String getMessage() {
        return String.format("Creation of Entity {%s} was Failed", aClass);
    }
}
