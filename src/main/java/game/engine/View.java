package game.engine;

import com.sun.xml.internal.bind.Locatable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@NoArgsConstructor
public class View  {

    @Setter
    protected long id;
    protected String name;
    protected int width;
    protected int height;

    public View(String name, int width, int height) {
        this.name = name;
        this.width = width;
        this.height = height;
    }
}
