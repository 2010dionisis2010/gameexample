package game.engine;

public interface Locatable {

    Location getLocation();
}
