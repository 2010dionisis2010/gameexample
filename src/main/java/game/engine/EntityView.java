package game.engine;

import lombok.Getter;

import javax.swing.*;
import java.awt.*;

public class EntityView extends View {

    @Getter
    private JLabel swingView = new JLabel();

    public <T extends Component> EntityView(String name, int width, int height) {
        super(name, width, height);
        this.width = width;
        this.height = height;
        swingView.setPreferredSize(new Dimension(getWidth(),getHeight()));
        this.name = name;
        swingView.setName(name);
        swingView.setBounds(10,10, width,height);
        swingView.setText(name);
    }

    public void updateLocation(int x, int y) {
        swingView.setLocation(x, y);
    }
}
