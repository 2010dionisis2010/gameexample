package game.client;

import game.dto.Event;
import game.dto.EventType;
import game.dto.Param;
import game.entity.game.Game;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class Launcher {

    private static ObjectInputStream serverReader;

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        log.info("client started");
        Socket server = new Socket("localhost", 8085);

        log.info("client connected");

        InputStream inputStream = server.getInputStream();
        serverReader = new ObjectInputStream(inputStream);

        Game game = getGameFromServer(serverReader);

        Renderer renderer = new Renderer();
        renderer.showGame(game);

        Thread stateUpdater = new Thread(() -> {
            Event event = readEventFromServer();
            if (event.getType() == EventType.UPDATE) {
                game.updateState((Game) event.getArgs().get(Param.GAME));
                renderer.rerender(game);
            }
        });
        stateUpdater.start();
        log.info("client stopped");

    }

    private static Event readEventFromServer()  {
        try {
            return (Event) serverReader.readObject();
        } catch (IOException | ClassNotFoundException e) {
            log.error("read from server was failed" , e);
            throw new IllegalStateException("read from server was failed", e);
        }
    }

    private static Game getGameFromServer(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        Game game = (Game) objectInputStream.readObject();
        log.info(String.format("Client got the game %d", game.getId()));
        return game;
    }
}
