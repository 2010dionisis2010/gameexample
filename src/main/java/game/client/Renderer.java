package game.client;

import game.engine.Location;
import game.engine.View;
import game.engine.ViewFactory;
import game.entity.Entity;
import game.entity.game.Game;
import game.entity.game.MainFrame;
import lombok.extern.log4j.Log4j2;

import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
public class Renderer {

    private ViewFactory  viewFactory = ViewFactory.MAIN_VIEW_FACTORY;
    private MainFrame mainFrame;

    public void showGame(Game game) {

        log.debug("rendering started");
        mainFrame = viewFactory.create(game);
        mainFrame.show();
    }

    public void rerender(Game game) {
        Map<Long, Entity> newEntities = game.getEntityList().stream().collect(Collectors.toMap(Entity::getId, entity -> entity));
        mainFrame.getEntityViewList()
                .forEach(entityView -> {
                    Location newLocation = newEntities.get(entityView.getId()).getLocation();
                    entityView.updateLocation(newLocation.getX(), newLocation.getY());
                });
        mainFrame.reRender();

    }


}
