package game.server;

import game.dto.Event;
import game.dto.Param;
import game.entity.game.Game;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;

import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Objects;

@Getter
@Log4j2
public class ClientConnection extends Thread {

    private static long nextID = 0;

    private long id;
    private Socket socket;
    private Game game;

    public ClientConnection(Socket socket, Game game) {
        this.socket = socket;
        this.game = game;
        this.id = ++nextID;
    }

    @SneakyThrows
    @Override
    public void run() {
        OutputStream outputStream = socket.getOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

        objectOutputStream.writeObject(game);
        objectOutputStream.flush();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientConnection that = (ClientConnection) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @SneakyThrows
    public void updateGameState() {
        OutputStream outputStream = socket.getOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

        Event updateEvent = new Event();
        updateEvent.put(Param.GAME, game);
        objectOutputStream.writeObject(updateEvent);
        objectOutputStream.flush();
    }
}
