package game.server;

import game.data.ConnectionFactory;
import game.data.dao.EntityDaoPostgres;
import game.data.dao.EntityRepo;
import game.entity.Configuration;
import game.entity.Entity;
import game.entity.game.Game;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Log4j2
public class Server {

    private Map<Long, ClientConnection> connectionPool = new HashMap<>();
    private Game game;
    private long iterationID = 0;

    public static void main(String[] args) throws IOException {
        Server server = new Server();
        log.info("Application started");

        server.start();
    }

    public void start() throws IOException {
        Thread.currentThread().setName("Main Thread");
        log.info("Server started");
        game = createNewGame();

        ServerSocket serverSocket = new ServerSocket(8085);
        startClientConnectionOpenner(serverSocket);
        startRandomMoving();

        startStateSaver();

    }

    private void startStateSaver() {
        EntityRepo entityRepo = new EntityRepo();
        Thread stateSaver = new Thread(() -> {
            for (Entity entity : game.getEntityList()) {
                entityRepo.save(entity);
            }
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                log.error(e);
            }
        });
        stateSaver.start();
    }

    private void startClientConnectionOpenner(ServerSocket serverSocket) {
        Thread connectionOpener = new Thread(() -> {
            while (true) {
                try {
                    Socket socket = serverSocket.accept();
                    ClientConnection clientConnection = new ClientConnection(socket, game);
                    connectionPool.put(clientConnection.getId(), clientConnection);
                    clientConnection.setDaemon(true);
                    clientConnection.start();
                } catch (IOException e) {
                    log.error("opening of connection failed ");
                }
            }
        });
        connectionOpener.setName("Connection opener");
        connectionOpener.start();
    }

    private void startRandomMoving() {
        Thread objectMover = new Thread(() -> {
            while (Thread.currentThread().isAlive()) {
                log.info("Random moving iteration " + ++iterationID);
                game.randomMoveAllEnemies();
                sendGameStateToAllClients();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    log.error(e);
                    break;
                }
            }
        });
        objectMover.setName("Object Mover");
        objectMover.start();
    }

    private void sendGameStateToAllClients() {
        connectionPool.values().stream()
                .filter(client -> !client.getSocket().isClosed())
                .forEach(ClientConnection::updateGameState);
    }

    private Game createNewGame() {
        Configuration aDefault = new Configuration(5, 500, 500);
        return new Game(aDefault);
    }


}
